#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <ctype.h>

#define _MAX_EVENTS 10 // Maximo 10 eventos
#define _MAX_DESCRIPTION 101 // 100 caracteres por descripcion

typedef struct { // Se define la estructura

    int hour; // Amacena la hora / HH
    int minute; // almacena los minutos / MM
    char description[_MAX_DESCRIPTION]; // Almacena la descripcion del evento

} event;

// Imprime el menu de seleccion
void printMenu() {

    puts("+------- SCHEDULER ------+\n"
        "|  1. Nueva Tarea         |\n"
        "|  2. Borrar Tarea        |\n"
        "|  3. Mostrar Tarea       |\n"
        "|  4. Exit                |\n"
        "+-------------------------+\n");

}

// Devuelve verdadero si un evento es NULO, falso de lo contrario
bool isNull(const event *e) { return e == NULL; }

// Asigna memoria e inicializar un evento.
event *initEvent() {
    event *e = (event*)malloc(sizeof(event));

    e->hour = 0;
    e->minute = 0;
    strcpy(e->description, "");

    return e;
}

// Toma la entrada del usuario hasta que el valor esté entre el min y max inclusive, devuelve la entrada
int inputRange(const int min, const int max) {

    int input = 0;
    char temp[21];
    char *prompt = "| Ingrese un numero entre %d and %d: ";

    printf(prompt, min, max);

    fgets(temp, 21, stdin);
    input = atoi(temp);

    while (input > max || input < min) { // Validación de datos
        printf(prompt, min, max);
        fgets(temp, 21, stdin);
        input = atoi(temp);
    }

    return input;

}

// Configura un nuevo evento con la entrada del usuario y devuelve un puntero al mismo evento
event* newEvent(event *e) {

    if (isNull(e)) { // If es nulo
        e = initEvent(); // Inicia it
    }

    char *seperator = "+--------------------------------+";

    printf("\n%s\n|           NUEVA TAREA            |\n%s\n\n", seperator, seperator);

    puts("+---------- TIEMPO DE LA TAREA ----------+");

    e->hour = inputRange(0, 23);
    e->minute = inputRange(0, 59);

    puts(seperator);

    puts("\n+--- DESCRIPCION DE LA TAREA ---+");

    printf("%s", "| Ingrese la descripcion de la tarea: ");

    fgets(e->description, _MAX_DESCRIPTION, stdin);

    puts("+-------------------------+\n");

    puts("| Tarea agregada.\n");

    return e;

}

// Agrega un evento a una lista de eventos en un índice específico
void addEventAtIndex(event list[], const event e, const int i) {

    if (isNull(&e)) { // si nuestro evento es NULL, regresa
        return;
    }

    list[i].hour = e.hour;
    list[i].minute = e.minute;
    strcpy(list[i].description, e.description);

}

// Ordenación por inserción intercambiando miembros de estructura
void sort(event list[], const int size) {

    for (int i = 1; i < size; i++) {
        for (int j = i; j > 0 && (list[j - 1].hour > list[j].hour || (list[j - 1].hour == list[j].hour && list[j - 1].minute > list[j].minute)); j--) {
            int hourJ = list[j].hour;
            int minuteJ = list[j].minute;
            char descriptionJ[_MAX_DESCRIPTION];
            strcpy(descriptionJ, list[j].description);

            int hourJMinus1 = list[j - 1].hour;
            int minuteJMinus1 = list[j - 1].minute;
            char descriptionJMinus1[_MAX_DESCRIPTION];
            strcpy(descriptionJMinus1, list[j - 1].description);

            list[j].hour = hourJMinus1;
            list[j].minute = minuteJMinus1;
            strcpy(list[j].description, descriptionJMinus1);

            list[j - 1].hour = hourJ;
            list[j - 1].minute = minuteJ;
            strcpy(list[j - 1].description, descriptionJ);
        }
    }

}

// Agrega un evento a una lista de eventos ordenándolo en posición
void sortInsert(event list[], int *size, event e) {

    addEventAtIndex(list, e, *size); // Agrega un evento al final de la lista

    (*size)++; // Incrementa el tamaño

    // Tipo de insersion
    sort(list, *size);

}

// Mostrar un evento en un formato legible: [ID] HH: MM - DESCRIPCIÓN
void printEvent(const event e) {

    char h1 = { (e.hour / 10) + '0' }; // Extract the first digit and convert to char (if any, else 0)
    char h2 = { (e.hour - (e.hour / 10) * 10) + '0' }; // Extract the second digit and convert to char

    char m1 = { (e.minute / 10) + '0' };
    char m2 = { (e.minute - (e.minute / 10) * 10) + '0' };

    printf("%c%c:%c%c - %s", h1, h2, m1, m2, e.description);

}

// Display all events in an event list
void printEventList(const event list[], const int size) {

    if (size == 0) {
        puts("\n| No hay tareas en el Scheduler!\n");
        return;
    }

    char *seperator = "+--------------------------------+";

    printf("\n%s\n|          MI SCHEDULER           |\n%s\n\n", seperator, seperator);

    for (int i = 0; i < size; i++) {
        printf("| [%d] ", i);
        printEvent(list[i]);

    }

    putchar('\n');

}

// Delete an event from an event list
void deleteEvent(event list[], int *size) {

    if (*size == 0) { // If list is empty
        puts("\n| Lista Vacia.\n");
        return;
    }

    char temp[21];
    int id;

    char *seperator = "\n+--------------------------------+";
    printf("%s\n|          BORRAR TAREA         |%s\n\n", seperator, seperator);

    for (int i = 0; i < *size; i++) { // Display the event list so the user can see which event to delete
        printf("| [%d] ", i);
        printEvent(list[i]);
    }

    printf("%s", "\n| Ingrese el ID de la tarea a borrar: ");

    fgets(temp, 21, stdin);
    id = atoi(temp);

    if (id > *size - 1) {
        printf("\n| Tarea no localizada en %d\n", id);
        return;
    }

    printf("| Tarea [%d] Borrada exitosamente \n\n", id);

    // Set hour and minute to some trivially large value for sorting purposes
    list[id].hour = 99;
    list[id].minute = 99;
    strcpy(list[id].description, "");

    if (id != (*size - 1)) { // If the event to remove is already last, there's no need to sort it to last
        sort(list, *size);
    }

    (*size)--; // Decrement the size of the list

}

// Replace all spaces in a string with an underscore
char *encode(char *s) {

    for (int i = 0; i < strlen(s); i++) {
        if (s[i] == ' ') {
            s[i] = '_';
        }
    }

    return s;

}

// Replace all underscores in a string with an spaces
char *decode(char *s) {

    for (int i = 0; i < strlen(s); i++) {
        if (s[i] == '_') {
            s[i] = ' ';
        }
    }

    return s;

}

// Save an event list to file
/*void saveEventList(char *filename, event list[], int size) {

    FILE *f = fopen(filename, "w");

    if (f == NULL) { // If our file is NULL, return
        return;
    }

    for (int i = 0; i < size; i++) {
        fprintf(f, "%d %d %s", list[i].hour, list[i].minute, encode(list[i].description)); // Encode the description (replace spaces with underscores) before saving it into the file
    }

    printf("\n| %d %s Guardado con exito en \"%s\".\n\n", size, (size == 1) ? "event" : "events", filename); // Tenary expression to make sure we're grammatically correct

    fclose(f);

}

// Load an event list from file
void loadEventList(char *filename, event list[], int *size) {

    FILE *f = fopen(filename, "r");
    char temp[6 + _MAX_DESCRIPTION]; // ## ## MAX_DESCRIPTION_LENGTH

    if (f == NULL) {
        printf("\n| Archivo \"%s\" no encontrado.\n\n", filename);
        return;
    }

    *size = 0; // Set size to 0

    while (fgets(temp, sizeof(temp), f)) {

        char *word = strtok(temp, " "); // Use space as the token delimiter, get the first token (hour)
        list[*size].hour = atoi(word); // Store the token into the list

        word = strtok(NULL, " "); // Get the second token (minute)
        list[*size].minute = atoi(word);

        word = strtok(NULL, " "); // Get the third token (description)
        strcpy(list[*size].description, decode(word)); // Decode our word before copying it (remove underscores)

        (*size)++; // Increment size with each line (event) added

    }

    printf("\n| %d %s cargado con éxito desde \"%s\".\n", *size, (*size == 1) ? "event" : "events", filename);

    printEventList(list, *size); // Display the event list when finished, show the user what's been loaded

}*/

int main() {

    event list[_MAX_EVENTS];
    int index = 0; // Number of elements in list
    int selection = 0;
    char file[FILENAME_MAX];
    char response = 'Y';
    char temp[21];

    while (selection != 6) {

        printMenu(); // Print the menu

        printf("%s", "| Por favor selecciona una opcion: "); // Prompt for input
        fgets(temp, 21, stdin);
        selection = atoi(temp); // Convert string input to int

        switch (selection) {

        case 1: // New Event
            if (index + 1 > _MAX_EVENTS) {
                printf("| Solo puede tener %d eventos activos a la vez!\n\n", index);
                break;
            }
            sortInsert(list, &index, *newEvent(&list[index]));
            break;
        case 2: // Delete Event
            deleteEvent(list, &index);
            break;
        case 3: // Display Schedule
            printEventList(list, index);
            break;
        /*case 4: // Save Schedule
            if (index == 0) { // No events, don't save anything
                puts("| No hay eventos en el Scheduler!\n");
            }
            else {
                printf("%s", "| Por favor ingrese un \"filename.txt\": ");
                fgets(file, FILENAME_MAX, stdin);
                strtok(file, "\n"); // Strip newline from filename
                saveEventList(file, list, index);
            }
            break;
        case : // Load Schedule
            if (index > 0) {
                printf("%s", "| ¿Estás seguro de que deseas descartar tu horario actual? (Y/N): ");
                response = toupper(getc(stdin));
                char c;
                while (((c = getchar()) != '\n') && (c != EOF)); // Clear buffer, from getc();
            }
            if (response == 'Y') {
                printf("%s", "| Por favor ingrese un \"filename.txt\": ");
                fgets(file, FILENAME_MAX, stdin);
                strtok(file, "\n"); // Strip newline from filename
                loadEventList(file, list, &index);
            }
            break;*/
        case 4: // Exit Program
            puts("\n| Gracias!\n");
            break;
        default: // Error
            puts("\n| Error en seleccion\n");
            break;

        }

    }

}
